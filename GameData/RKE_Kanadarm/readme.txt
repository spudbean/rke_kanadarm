RKE Kanadarm
version @RKEVERSION@

Forum Thread:  http://forum.kerbalspaceprogram.com/threads/112881
Source code:   https://bitbucket.org/spudbean/rke_kanadarm
License:       Creative Commons Attribution 4.0 International

Installation
============

1. Unzip
2. Copy RKE_Kanadarm into <KSP_OS>\GameData
   (Delete existing RKE_Kanadarm directory if it exists.)


Copyright
=========

Copyright 2015 Matt Quail

This work is licensed under a Creative Commons Attribution 4.0 International
License. http://creativecommons.org/licenses/by/4.0/

Contains audio files copyright qubodup, http://creativecommons.org/licenses/by/3.0/